import contextlib

import anyio
import tiny_proxy

import logging  # isort:skip
_log = logging.getLogger(__name__)

HOST = 'localhost'
PORT = 60100
HANDLERS = {
    'http': {
        'handler_cls': tiny_proxy.HttpProxyHandler,
        'scheme': 'http',
        'host': HOST,
        'port': PORT,
    },
    'socks4': {
        'handler_cls': tiny_proxy.Socks4ProxyHandler,
        'scheme': 'socks4',
        'host': HOST,
        'port': PORT,
    },
    'socks5': {
        'handler_cls': tiny_proxy.Socks5ProxyHandler,
        'scheme': 'socks5',
        'host': HOST,
        'port': PORT,
    },
}


async def _create_proxy(handler, host, port, task_status):
    async with await anyio.create_tcp_listener(
            local_host=host,
            local_port=port,
    ) as listener:
        task_status.started()
        await listener.serve(handler.handle)


async def _proxy_context_manager(handler_name):
    handler = HANDLERS[handler_name]['handler_cls']()
    scheme = HANDLERS[handler_name]['scheme']
    host = HANDLERS[handler_name]['host']
    port = HANDLERS[handler_name]['port']
    url = f'{scheme}://{host}:{port}'

    async with anyio.create_task_group() as tg:
        await tg.start(_create_proxy, handler, host, port)
        _log.debug('Proxy started: %r: %s', handler, url)
        yield url
        tg.cancel_scope.cancel()
        _log.debug('Proxy stopped: %r: %s', handler, url)


@contextlib.asynccontextmanager
def http_proxy():
    return _proxy_context_manager('http')

@contextlib.asynccontextmanager
def socks4_proxy():
    return _proxy_context_manager('socks4')

@contextlib.asynccontextmanager
def socks5_proxy():
    return _proxy_context_manager('socks5')


proxies = (
    http_proxy,
    socks4_proxy,
    socks5_proxy,
)
